CREATE TABLE superhero (id BIGINT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) NOT NULL);
INSERT INTO superhero (name) VALUES ('Superman');
INSERT INTO superhero (name) VALUES ('Spiderman');
INSERT INTO superhero (name) VALUES ('Thor');