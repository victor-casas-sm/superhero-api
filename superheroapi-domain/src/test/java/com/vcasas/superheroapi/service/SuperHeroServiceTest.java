package com.vcasas.superheroapi.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.vcasas.superheroapi.error.SuperHeroNotFoundException;
import com.vcasas.superheroapi.model.SuperHero;
import com.vcasas.superheroapi.outbound.SuperHeroProvider;

@ExtendWith(MockitoExtension.class)
public class SuperHeroServiceTest {

    @Mock
    SuperHeroProvider repo;
    @InjectMocks
    SuperHeroServiceImpl service;

    @Test
    void getSuperHeroesContainingWord_ShouldReturnMatchingHeroes() {
        String wordToSearch = "man";
        when(repo.findByNameContainingIgnoreCase(wordToSearch))
          .thenReturn(List.of(SuperHero.of(1L, "Superman")));
    
        List<SuperHero> result = service.getSuperHeroesContainingAWord(wordToSearch);

        assertNotNull(result);
        assertTrue(!result.isEmpty());
        assertTrue(result.stream().allMatch(superHero -> superHero.getName().toUpperCase().contains(wordToSearch.toUpperCase())));
    }
    
    @Test
    public void getSuperHeroesContainingWord_ShouldThrowExceptionWhenNoHeroesFound(){
        when(repo.findByNameContainingIgnoreCase(anyString()))
          .thenReturn(List.of());

        assertThrows(SuperHeroNotFoundException.class, () -> service.getSuperHeroesContainingAWord(anyString()));
    }
}
