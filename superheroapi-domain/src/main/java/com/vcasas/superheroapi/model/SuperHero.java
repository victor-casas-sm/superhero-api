package com.vcasas.superheroapi.model;

import lombok.Value;

@Value(staticConstructor = "of")
public class SuperHero {
    private Long id;
    private String name;
}
