package com.vcasas.superheroapi.error;

public class SuperHeroNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 8395887007263666961L;

    public SuperHeroNotFoundException(String message) {
        super(message);
    }

    public SuperHeroNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
