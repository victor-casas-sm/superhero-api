package com.vcasas.superheroapi.service;

import java.util.List;
import java.util.Optional;

import com.vcasas.superheroapi.model.SuperHero;

public interface SuperHeroService {
    List<SuperHero> getSuperHeroesContainingAWord(String word);
    List<SuperHero> getAllSuperHeroes();
    Optional<SuperHero> getSuperHeroById(Long id);
    SuperHero createSuperHero(SuperHero superHero);
    SuperHero updateSuperHero(Long id, SuperHero superHero);
    void deleteSuperHero(Long id);

}
