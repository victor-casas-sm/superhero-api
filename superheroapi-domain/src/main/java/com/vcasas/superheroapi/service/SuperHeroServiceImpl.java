package com.vcasas.superheroapi.service;

import java.util.List;
import java.util.Optional;

import com.vcasas.superheroapi.error.SuperHeroNotFoundException;
import com.vcasas.superheroapi.model.SuperHero;
import com.vcasas.superheroapi.outbound.SuperHeroProvider;

public class SuperHeroServiceImpl implements SuperHeroService {
    private static final String NOT_FOUND_MESSAGE_FORMAT = "The superhero witn name containing word <%s> has not been found";
    private final SuperHeroProvider repo;

    public SuperHeroServiceImpl(SuperHeroProvider repo) {
        this.repo = repo;
    }

    @Override
    public List<SuperHero> getSuperHeroesContainingAWord(String word) {
        return Optional.of(repo.findByNameContainingIgnoreCase(word))
            .filter(list -> !list.isEmpty()) 
            .orElseThrow(() -> new SuperHeroNotFoundException(String.format(NOT_FOUND_MESSAGE_FORMAT, word)));
    }

    @Override
    public List<SuperHero> getAllSuperHeroes() {
        return repo.getAllSuperHeroes();
    }

    @Override
    public Optional<SuperHero> getSuperHeroById(Long id) {
        return repo.getSuperHeroById(id);
    }

    @Override
    public SuperHero createSuperHero(SuperHero superHero) {
        return repo.createSuperHero(superHero);
    }
    @Override
    public SuperHero updateSuperHero(Long id, SuperHero superHero) {

        Optional<SuperHero> existingSuperHeroOptional = repo.getSuperHeroById(id);
        if (existingSuperHeroOptional.isPresent()) {
            return repo.updateSuperHero(id, superHero);
        } else {
            throw new SuperHeroNotFoundException("SuperHero not found with id: " + id);
        }
    }

    @Override
    public void deleteSuperHero(Long id) {
        repo.deleteSuperHero(id);
    }
}