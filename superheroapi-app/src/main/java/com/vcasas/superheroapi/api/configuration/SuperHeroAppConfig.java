package com.vcasas.superheroapi.api.configuration;

import com.vcasas.superheroapi.outbound.SuperHeroProvider;
import com.vcasas.superheroapi.service.SuperHeroService;
import com.vcasas.superheroapi.service.SuperHeroServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SuperHeroAppConfig implements WebMvcConfigurer {

    @Bean
	public SuperHeroService superHeroService(SuperHeroProvider prov) {
		return new SuperHeroServiceImpl(prov);
	}

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // Permitir CORS para todas las rutas
        registry.addMapping("/**")
                .allowedOrigins("https://frontend-angular-e5c5.onrender.com")  // URL de tu frontend
                .allowedMethods("GET", "POST", "PUT", "DELETE")  // Métodos permitidos
                .allowedHeaders("*");  // Permitir todos los encabezados
    }

}
