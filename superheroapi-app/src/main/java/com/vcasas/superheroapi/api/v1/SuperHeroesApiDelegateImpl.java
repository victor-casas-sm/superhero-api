package com.vcasas.superheroapi.api.v1;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.vcasas.superheroapi.error.SuperHeroNotFoundException;
import com.vcasas.superheroapi.model.SuperHero;
import com.vcasas.superheroapi.service.SuperHeroService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SuperHeroesApiDelegateImpl implements SuperheroesApiDelegate {
    private final SuperHeroService domain;

    public SuperHeroesApiDelegateImpl(SuperHeroService domain) {
        this.domain = domain;
    }

    @Override
    public ResponseEntity<List<com.vcasas.superheroapi.model.v1.SuperHero>> getSuperHeroesContainingWord(String word) {
        List<SuperHero> superHeroes = domain.getSuperHeroesContainingAWord(word);
        
    List<com.vcasas.superheroapi.model.v1.SuperHero> openApiSuperHeroes = superHeroes.stream()
            .map(entity -> new com.vcasas.superheroapi.model.v1.SuperHero()
                    .id(entity.getId())
                    .name(entity.getName())
            )
            .collect(Collectors.toList());
    
    return Optional.ofNullable(openApiSuperHeroes)
            .filter(list -> !list.isEmpty())
            .map(ResponseEntity::ok)
            .orElseThrow(() -> new SuperHeroNotFoundException("Superhero not found"));
    }
    
    @Override
    public ResponseEntity<List<com.vcasas.superheroapi.model.v1.SuperHero>> getAllSuperHeroes() {
        List<SuperHero> superHeroes = domain.getAllSuperHeroes();

        List<com.vcasas.superheroapi.model.v1.SuperHero> openApiSuperHeroes = superHeroes.stream()
                .map(entity -> new com.vcasas.superheroapi.model.v1.SuperHero()
                        .id(entity.getId())
                        .name(entity.getName())
                )
                .collect(Collectors.toList());

        return Optional.ofNullable(openApiSuperHeroes)
                .filter(list -> !list.isEmpty())
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new SuperHeroNotFoundException("Superhero not found"));
    }

    @Override
    public ResponseEntity<com.vcasas.superheroapi.model.v1.SuperHero> createSuperHero(com.vcasas.superheroapi.model.v1.SuperHero superHero) {
        SuperHero createdSuperHero = domain.createSuperHero(SuperHero.of(superHero.getId(), superHero.getName()));

        com.vcasas.superheroapi.model.v1.SuperHero createdApiSuperHero = new com.vcasas.superheroapi.model.v1.SuperHero()
                .id(createdSuperHero.getId())
                .name(createdSuperHero.getName());

        return ResponseEntity.ok(createdApiSuperHero);
    }

    @Override
    public ResponseEntity<com.vcasas.superheroapi.model.v1.SuperHero> getSuperHeroById(Long id) {
        Optional<SuperHero> optionalSuperHero = domain.getSuperHeroById(id);
        SuperHero superHero = optionalSuperHero.orElseThrow(() -> new SuperHeroNotFoundException("Superhero not found with id: " + id));

        com.vcasas.superheroapi.model.v1.SuperHero apiSuperHero = new com.vcasas.superheroapi.model.v1.SuperHero()
                .id(superHero.getId())
                .name(superHero.getName());

        return ResponseEntity.ok(apiSuperHero);
    }

    @Override
    public ResponseEntity<com.vcasas.superheroapi.model.v1.SuperHero> updateSuperHero(Long id, com.vcasas.superheroapi.model.v1.SuperHero superHero) {
        domain.updateSuperHero(id, SuperHero.of(id, superHero.getName()));
        com.vcasas.superheroapi.model.v1.SuperHero apiSuperHero = new com.vcasas.superheroapi.model.v1.SuperHero()
        .id(superHero.getId())
        .name(superHero.getName());
        return ResponseEntity.ok(apiSuperHero);
    }

    @Override
    public ResponseEntity<Void> deleteSuperHero(Long id) {
        domain.deleteSuperHero(id);
        return ResponseEntity.noContent().build();
    }
}
