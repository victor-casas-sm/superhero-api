package com.vcasas.superheroapi.api.v1;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.*;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;

import com.vcasas.superheroapi.model.v1.SuperHero;
import com.vcasas.superheroapi.error.SuperHeroNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@WebMvcTest(SuperheroesApiController.class)
public class SuperheroesApiControllerTest {
    
    @Autowired
    MockMvc mockMvc;

    @MockBean
    SuperheroesApiDelegate delegate;

    @Test
    public void getSuperHeroesContainingWord_ShouldBeOK() throws Exception {
        when(delegate.getSuperHeroesContainingWord(anyString()))
          .thenReturn(ResponseEntity.ok(List.of(new SuperHero("Superman"))));

        mockMvc.perform(MockMvcRequestBuilders
          .get("/superheroes/search?word=man")
          .contentType(MediaType.APPLICATION_JSON))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$[0].name", is("Superman")));
    }

    @Test
    public void getSuperHeroesContainingWord_ShouldBeNotFound() throws Exception {
        when(delegate.getSuperHeroesContainingWord(anyString()))
          .thenThrow(new SuperHeroNotFoundException("not found"));

        mockMvc.perform(MockMvcRequestBuilders
            .get("/superheroes/search?word=man")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    public void getSuperHeroesContainingWord_RespondingException_ShouldBeInternaServerlError() throws Exception {
        when(delegate.getSuperHeroesContainingWord(anyString()))
          .thenThrow(new RuntimeException("great exception"));

        mockMvc.perform(MockMvcRequestBuilders
            .get("/superheroes/search?word=man")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isInternalServerError());

    }
}