package com.vcasas.superheroapi.api.v1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import com.vcasas.superheroapi.error.SuperHeroNotFoundException;
import com.vcasas.superheroapi.model.SuperHero;
import com.vcasas.superheroapi.service.SuperHeroService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class SuperHeroApiDelegateTest {
    private SuperHeroService service = mock(SuperHeroService.class);
    private SuperheroesApiDelegate delegate = new SuperHeroesApiDelegateImpl(service);

    @SuppressWarnings("null")
    @Test
    public void getSuperHeroesContainingWord() throws Exception {
        String wordToSearch = "man";
        when(service.getSuperHeroesContainingAWord(wordToSearch))
          .thenReturn(List.of(SuperHero.of(1L, "Superman")));

        ResponseEntity<List<com.vcasas.superheroapi.model.v1.SuperHero>> response = delegate.getSuperHeroesContainingWord("man");
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().size());
        assertEquals(1L, response.getBody().get(0).getId());
        assertEquals("Superman", response.getBody().get(0).getName());
        assertTrue(response.getBody().get(0).getName().toUpperCase().contains(wordToSearch.toUpperCase()));

    }

    @Test
    public void pricesByDateNotFoundTest() throws Exception {
        when(service.getSuperHeroesContainingAWord(any())).
          thenReturn(List.of());
        assertThrows(
            SuperHeroNotFoundException.class,
            () -> delegate.getSuperHeroesContainingWord(any())
        );
     }

}
