package com.vcasas.superheroapi.persistence.respository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.vcasas.superheroapi.persistence.PersistenceConfiguration;
import com.vcasas.superheroapi.persistence.entities.SuperHeroEntity;
import com.vcasas.superheroapi.persistence.repositories.SuperHeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = PersistenceConfiguration.class)
class SuperHeroRepositoryTest {

  @Autowired
  private SuperHeroRepository repository;

  @Test
  void findByNameContainingIgnoreCase() {

    String search = "man";

    List<SuperHeroEntity> result = repository.findByNameContainingIgnoreCase(search);

    assertNotNull(result);
    assertTrue(!result.isEmpty());
    assertTrue(result.size() == 2);
    assertTrue(result.stream().allMatch(superHero -> superHero.getName().toUpperCase().contains(search.toUpperCase())));
  }
}