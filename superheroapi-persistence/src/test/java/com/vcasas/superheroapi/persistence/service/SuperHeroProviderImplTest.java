package com.vcasas.superheroapi.persistence.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.vcasas.superheroapi.model.SuperHero;
import com.vcasas.superheroapi.persistence.entities.SuperHeroEntity;
import com.vcasas.superheroapi.persistence.repositories.SuperHeroRepository;

@ExtendWith(MockitoExtension.class)

class SuperHeroProviderImplTest {
  @Mock
  SuperHeroRepository repo;
  @InjectMocks
  SuperHeroProviderImpl provider;

  @Test
  void findByNameContainingIgnoreCase_ShouldReturnMatchingHeroes() {
    String wordToSearch = "man";
    when(repo.findByNameContainingIgnoreCase(wordToSearch))
      .thenReturn(List.of(new SuperHeroEntity(1L, "Superman")));
    

    List<SuperHero> result = provider.findByNameContainingIgnoreCase(wordToSearch);
    assertNotNull(result);
    assertTrue(!result.isEmpty());
    assertTrue(result.stream().allMatch(superHero -> superHero.getName().toUpperCase().contains(wordToSearch.toUpperCase())));
  }
}