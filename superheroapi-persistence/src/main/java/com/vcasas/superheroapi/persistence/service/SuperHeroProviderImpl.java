package com.vcasas.superheroapi.persistence.service;

import java.util.List;
import java.util.Optional;

import com.vcasas.superheroapi.persistence.annotation.ExecutionTime;
import com.vcasas.superheroapi.persistence.entities.SuperHeroEntity;
import com.vcasas.superheroapi.error.SuperHeroNotFoundException;
import com.vcasas.superheroapi.model.SuperHero;
import com.vcasas.superheroapi.outbound.SuperHeroProvider;
import com.vcasas.superheroapi.persistence.repositories.SuperHeroRepository;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class SuperHeroProviderImpl implements SuperHeroProvider {
    private final SuperHeroRepository dbRepo;

    public SuperHeroProviderImpl(SuperHeroRepository dbRepo){
        this.dbRepo = dbRepo;
    }

    @ExecutionTime
    @Override
    public List<SuperHero> findByNameContainingIgnoreCase(String word) {
        return this.dbRepo.findByNameContainingIgnoreCase(word).stream()
        .map(e -> SuperHero.of(
            e.getId(), e.getName()
        )).toList();
    }

    @Override
    public List<SuperHero> getAllSuperHeroes() {
        List<SuperHeroEntity> entities = this.dbRepo.findAll();
        return entities.stream()
                  .map(entity -> SuperHero.of(entity.getId(), entity.getName()))
                  .toList();
    }

    @Override
    public SuperHero createSuperHero(SuperHero superHero) {
        SuperHeroEntity entity = new SuperHeroEntity();
        entity.setName(superHero.getName());
        SuperHeroEntity savedEntity = this.dbRepo.save(entity);
        return SuperHero.of(savedEntity.getId(), savedEntity.getName());
    }

    @Override
    public Optional<SuperHero> getSuperHeroById(Long id) {
        Optional<SuperHeroEntity> optionalEntity = this.dbRepo.findById(id);
        return optionalEntity.map(entity -> {
            SuperHero superHero = SuperHero.of(entity.getId(), entity.getName());
            return superHero;
        });
    }

    @Override
    public SuperHero updateSuperHero(Long id, SuperHero superHero) {
        Optional<SuperHeroEntity> optionalEntity = this.dbRepo.findById(id);
        if (optionalEntity.isPresent()) {
            SuperHeroEntity existingEntity = optionalEntity.get();
            existingEntity.setName(superHero.getName());
            SuperHeroEntity updatedEntity = this.dbRepo.save(existingEntity);
            return SuperHero.of(updatedEntity.getId(), updatedEntity.getName());
        } else {
            throw new SuperHeroNotFoundException("SuperHero not found with id: " + id);
        }
    }

    @Override
    public void deleteSuperHero(Long id) {

        if (this.dbRepo.existsById(id)) {
            this.dbRepo.deleteById(id);
        } else {
            throw new SuperHeroNotFoundException("SuperHero not found with id: " + id);
        }
    }
    
}
