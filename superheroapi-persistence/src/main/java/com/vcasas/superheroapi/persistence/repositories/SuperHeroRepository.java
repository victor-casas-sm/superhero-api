package com.vcasas.superheroapi.persistence.repositories;

import java.util.List;

import com.vcasas.superheroapi.persistence.entities.SuperHeroEntity;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
@Cacheable("superheroesByNameContainingAWord")
public interface SuperHeroRepository extends JpaRepository<SuperHeroEntity, Long> {   
     
    List<SuperHeroEntity> findByNameContainingIgnoreCase(String word);
}