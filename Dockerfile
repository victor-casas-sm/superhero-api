FROM maven:3.9.6-eclipse-temurin-21-jammy as build
WORKDIR /workspace

COPY superheroapi-domain superheroapi-domain
COPY superheroapi-persistence superheroapi-persistence
COPY superheroapi-app superheroapi-app
COPY pom.xml .

RUN mvn clean package -DskipTests -ntp

FROM eclipse-temurin:21-jre-jammy
WORKDIR /

COPY --from=build /workspace/superheroapi-app/target/*.jar /app.jar

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]
