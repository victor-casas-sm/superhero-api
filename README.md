# Superhero API

## Objetivo
El objetivo de este projecto es dar respuesta al reto propuesto en la [descripción adjunta](#el-problema-propuesto), implementando el software necesario, describiendo las decisiones tomadas tanto arquitectónicas como de implementación, y mostrando posibles alternativas y mejoras a aplicar en posteriores iteracciones de desarrollo.
> ***Nota**: Dado el tamaño del reto propuesto, el diseño realizado podría haber sido más "sencillo". Sin embargo, se ha intentado aplicar buenas prácticas de arquitectura, herramientas de uso común en el desarrollo y, en definitiva, ir un paso más allá de la mera resolución del mismo.*
## Implementación de la prueba
### Arquitectura aplicada

Partiendo de la definición del dominio (Entidades, ValueObjects y lógica de negocio), se han implementado los "puertos" y "adaptadores" necesarios para permitir la comunicación de ese dominio con el "mundo exterior" (persistencia y API/REST de consulta).  
Para ello, se ha modulado la solución teniendo en cuenta que toda la lógica de negocio resida en el módulo del dominio, poniendo especial cuidado en que dicho módulo no dependa de ningún otro, y dejando los detalles técnicos (frameworks, librerías, canales de comunicación, adaptación de mensajes) en los adaptadores necesarios.

### Diseño
Partiendo de un diseño habitual en una arquitectura hexagonal:  
![Arquitectura Hexagonal](./design/images/hexagonal.png)

Se propone una solución con un único dominio (superheroapi-domain) y un par de puertos para ofrecer funcionalidades de consulta (superheroapi-app) y recuperación de datos (superheroapi-persistence)  
Los adaptadores que implementarán estos puertos serán un API/REST y un repositorio de acceso a base de datos JPA.   
* Para el API/REST se utiliza un enfoque API-first, por lo que algunos de los elementos del adaptador vendrán dados a partir de la definición del API (OpenAPI). Para evitar "tocar" los elementos generados, se utilizará un patrón *"delegation"*.   
Para formatear el contenido de los mensajes de error, se utiliza un "[RestControllerAdvice](https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#mvc-ann-controller-advice)" de Spring.   
* Para el acceso a los datos se utiliza un repositorio de spring-data que consultará una entidad de base de datos.

![Diseño Propuesto](./design/images/superheroapi.png)

### Modulación
A partir del diseño propuesto, se plantea el desarrollo en tres módulos diferenciados (descritos en sus própios ficheros README.md):
* [Módulo de dominio](superheroapi-domain/README.md) 
* [Módulo de infraestructura](superheroapi-persistence/README.md). Contendrá el adaptador que implementa la persistencia en base de datos SQL
* [Módulo de aplicación](superheroapi-app/README.md). Contendrá el adaptador que implementa el API/REST de consulta

### Desarrollo
Para la implementación de la prueba se ha utilizado Java versión 17 (última versión LTS) y el sistema de construcción usado es Maven (última versión)
> ***Nota**: Para evitar la instalación de diversas herramientas en cliente, se puede utilizar un sistema de contenedores para la construcción y realización de pruebas, por lo que sólo sería necesario tener instalado Docker (o alguna otra herramienta similar como podman)*  
*Así, para la construcción de la prueba se realizaría con*
>```console
>docker build -f Dockerfile . -t prueba-java
>```
Se utilizará un projecto maven multi módulo donde se establecerán (y forzarán) las diferentes dependencias entre ellos y con los elementos técnicos (frameworks, librerías, etc.)  
El módulo de dominio (superheroapi-domain) no tendrá dependencias externas, forzándolo a través del plugin maven [enforce-plugin](https://maven.apache.org/enforcer/maven-enforcer-plugin/), y el resto (superheroapi-app y superheroapi-persistence) dependerán de él. 
## Pruebas integradas
Para la comprobación general del desarrollo se realizan pruebas integradas utilizando colecciones [Postman](https://www.postman.com/) que serán automatizadas mediante el uso de la herramienta newman.
Estas pruebas realizarán llamadas API/REST al servicio desarrollado comprobando que el contenido de las respuestas son las esperadas.
La descripción de las pruebas realizadas, así como las instrucciones para su ejecución se pueden encontrar en un [README.md](integration-tests/README.md) específico
## Mejoras implementadas sobre el problema propuesto
En la implementación de la prueba se han aplicado varias mejoras sobre el simple desarrollo y cumplimiento de los puntos a valorar propuestos.
* Se incluyen pruebas unitarias de todos los desarrollos implementados, siguiendo una aproximación TDD
* Se implementan pruebas de integración automatizadas
* La utilización de contenedores en el empaquetado permite la utilización directa del desarrollo en un entorno de orquestación tipo kubernetes
* Se utiliza spring-data y JPA que independiza el desarrollo de la tecnología de base de datos a utilizar.
* Se utiliza una aproximación, para la generación del API/REST, que se inicia desde la definición estándar de su interfaz utilizando **"[Openapi](https://swagger.io/specification/)"* *(API First)*
* La definición de un servidor API/REST tipo MOCK, permitiría el trabajo en paralelo de clientes, antes de la finalización del desarrollo
* La definición y uso de docker-compose permite crear un entorno de desarrollo y/o pruebas sin necesidad de instalar herramientas en entornos locales
* Se implementa una anotación personalizada para medir cuánto tarda en ejecutarse una petición. que funciona de forma similar al @Timed de Spring, pero imprimiendo la duración en un log.
* Se implementa un sistema de cache con sobre las consultas realizadas, con [Caffeine](https://github.com/ben-manes/caffeine/)
## Otras mejoras no implementadas
Auque se ha intentado ir "un paso más allá", aún quedan muchas otras mejoras que podrían aplicarse sobre esta implementación. Entre ellas:
* El uso de alguna librería de "mapeo" de atributos entre clases *([mapstruct?](https://mapstruct.org/))* para facilitar el desarrollo y aumentar la legibilidad del código dedicado a este fin.
* La implementación del API utilizando gRPC y protobuf
* ...

Pero los recursos nunca son infinitos...

## El problema propuesto
---
~~~
Prueba técnica Spring Boot
Importante:
Se debe utilizar la última versión LTS de Java, Spring Boot y de cualquier librería utilizada en el
proyecto.
Desarrollar, utilizando Maven, Spring Boot, y Java, una API que permita hacer un mantenimiento CRUD
de súper héroes.
Este mantenimiento debe permitir:
• Consultar todos los súper héroes.
• Consultar un único súper héroe por id.
• Consultar todos los súper héroes que contienen, en su nombre, el valor de un parámetro
enviado en la petición. Por ejemplo, si enviamos “man” devolverá “Spiderman”, “Superman”,
“Manolito el fuerte”, etc.
• Crear un súper héroe.
• Modificar un súper héroe.
• Eliminar un súper héroe.
• Test unitarios de como mínimo un servicio.
Puntos a tener en cuenta:
• Los súper héroes se deben guardar en una base de datos. Puede ser, por ejemplo, H2 en
memoria.
• La prueba se debe presentar en un repositorio de Git. No hace falta que esté publicado. Se
puede enviar comprimido en un único archivo.
Puntos opcionales de mejora:
• Utilizar alguna librería que facilite el mantenimiento de los scripts DDL de base de datos.
• Paginación.
• Implementar una anotación personalizada que sirva para medir cuánto tarda en ejecutarse.
una petición. Se podría anotar alguno o todos los métodos de la API con esa anotación.
Funcionaría de forma similar al @Timed de Spring, pero imprimiendo la duración en un log.
• Gestión centralizada de excepciones.
• Test de integración.
• Presentar la aplicación dockerizada.
• Poder cachear peticiones.
• Documentación de la API.
• Seguridad del API.
Se valorará positivamente:
• El uso de TDD. En caso de subir la práctica a un repositorio, se pueden utilizar los commits
para ver el proceso.
• Aplicación de los principios SOLID.
~~~
